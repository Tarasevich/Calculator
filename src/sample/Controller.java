package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

public class Controller {

    @FXML
    private TextField display;
    private int decimalClick = 0;
    private String generalOperationObject;
    private double firstDouble;
    private double secondDouble;
    private double resault;

    @FXML
    public void handleOperatorAction(ActionEvent event) {
         generalOperationObject = ((Button)event.getSource()).getText();
        switch(generalOperationObject){
            case "AC":
                display.setText("");
                decimalClick = 0;
                break;
            case"+/-":
                double plusMinus = Double.parseDouble(String .valueOf(display.getText()));
                plusMinus = plusMinus * (-1);
                display.setText(String.valueOf(plusMinus));
                break;
            case "+":
            case "-":
            case "*":
            case "/":
                String currentText = display.getText();
                firstDouble = Double.parseDouble(currentText);
                display.setText("");
                decimalClick = 0;
                break;
        }
        
    }
    @FXML
    public void handleDigitAction(ActionEvent event) {
        String digitObject =((Button)event.getSource()).getText();
        String oldText = display.getText();
        String newText = oldText + digitObject;
        display.setText(newText);
    }
    @FXML
    public void handleEqualAction(ActionEvent event) {
        String secontText = display.getText();
        secondDouble = Double.parseDouble(secontText);

        switch (generalOperationObject){
            case "+":
                resault = firstDouble + secondDouble;
                break;
            case "-":
                resault = firstDouble - secondDouble;
                break;
            case "*":
                resault = firstDouble * secondDouble;
                break;
            case "/":
                resault = firstDouble / secondDouble;
                break;
        }
        String format = String.format("%.1f",resault);
        display.setText(format);
    }
    @FXML
    public void handleDecimalAction(ActionEvent event) {
        if(decimalClick==0){
            String decimalObject =((Button)event.getSource()).getText();
            String oldText = display.getText();
            String newText = oldText + decimalObject;
            display.setText(newText);
            decimalClick = 1;
        }
    }
}
